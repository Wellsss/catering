﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Catering.Migrations
{
    public partial class Add_Message : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Remark",
                table: "meal");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Remark",
                table: "meal",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
