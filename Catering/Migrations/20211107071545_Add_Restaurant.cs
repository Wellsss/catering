﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Catering.Migrations
{
    public partial class Add_Restaurant : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_meal_resturant_RestaurantEntityId",
                table: "meal");

            migrationBuilder.RenameColumn(
                name: "RestaurantEntityId",
                table: "meal",
                newName: "RestaurantId1");

            migrationBuilder.RenameIndex(
                name: "IX_meal_RestaurantEntityId",
                table: "meal",
                newName: "IX_meal_RestaurantId1");

            migrationBuilder.AddColumn<long>(
                name: "RestaurantId",
                table: "meal",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_meal_RestaurantId",
                table: "meal",
                column: "RestaurantId");

            migrationBuilder.AddForeignKey(
                name: "FK_meal_resturant_RestaurantId",
                table: "meal",
                column: "RestaurantId",
                principalTable: "resturant",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_meal_resturant_RestaurantId1",
                table: "meal",
                column: "RestaurantId1",
                principalTable: "resturant",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_meal_resturant_RestaurantId",
                table: "meal");

            migrationBuilder.DropForeignKey(
                name: "FK_meal_resturant_RestaurantId1",
                table: "meal");

            migrationBuilder.DropIndex(
                name: "IX_meal_RestaurantId",
                table: "meal");

            migrationBuilder.DropColumn(
                name: "RestaurantId",
                table: "meal");

            migrationBuilder.RenameColumn(
                name: "RestaurantId1",
                table: "meal",
                newName: "RestaurantEntityId");

            migrationBuilder.RenameIndex(
                name: "IX_meal_RestaurantId1",
                table: "meal",
                newName: "IX_meal_RestaurantEntityId");

            migrationBuilder.AddForeignKey(
                name: "FK_meal_resturant_RestaurantEntityId",
                table: "meal",
                column: "RestaurantEntityId",
                principalTable: "resturant",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
