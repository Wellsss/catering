﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Catering.Migrations
{
    public partial class Update_SqlServerDbContext : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_meal_resturant_RestaurantId1",
                table: "meal");

            migrationBuilder.DropIndex(
                name: "IX_meal_RestaurantId1",
                table: "meal");

            migrationBuilder.DropColumn(
                name: "RestaurantId1",
                table: "meal");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "RestaurantId1",
                table: "meal",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_meal_RestaurantId1",
                table: "meal",
                column: "RestaurantId1");

            migrationBuilder.AddForeignKey(
                name: "FK_meal_resturant_RestaurantId1",
                table: "meal",
                column: "RestaurantId1",
                principalTable: "resturant",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
