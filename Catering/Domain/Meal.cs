﻿using Catering.Domain;

namespace Catering.Domain
{
    public class Meal
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Score { get; set; }
        public string Message { get; set; }
        public long RestaurantId { get; set; }

        public Restaurant Restaurant { get; set; }
    }
}