﻿using System.Collections.Generic;

namespace Catering.Domain
{
    public class Restaurant
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public List<Meal> Meals { get; } = new();

        public void AddMeal(Meal meal)
        {
            Meals.Add(meal);
        }
    }
}