﻿using Catering.Domain;
using Microsoft.EntityFrameworkCore;

namespace Catering.Infrastructure.DbContext
{
    public class SqlServerDbContext : Microsoft.EntityFrameworkCore.DbContext
    {
        public SqlServerDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Meal> MealSet { get; set; }
        public DbSet<Restaurant> RestaurantSet { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Meal>(e =>
            {
                e.ToTable(TableName.Meal);
                e.HasKey(o => new {o.Id});
            });
            modelBuilder.Entity<Domain.Restaurant>(e =>
            {
                e.ToTable(TableName.Resturant);
                e.HasKey(o => new {o.Id});
            });
        }
    }
}