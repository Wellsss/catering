﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Catering.Domain;
using Catering.Infrastructure.DbContext;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace Catering.Pages
{
    public class EditMealModel : PageModel
    {
        private readonly SqlServerDbContext _dbContext;


        public EditMealModel(SqlServerDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        [BindProperty] public Meal Meal { get; set; }

        public async Task<ActionResult> OnGetAsync(long? id)
        {
            if (!id.HasValue)
            {
                return NotFound();
            }

            Meal = await _dbContext.MealSet
                .Include(x => x.Restaurant)
                .SingleOrDefaultAsync(x => x.Id == id.Value);
            
            if (Meal is null)
            {
                return NotFound();
            }

            return Page();
        }

        public async Task<ActionResult> OnPostAsync()
        {
            _dbContext.Update(Meal);
            await _dbContext.SaveChangesAsync();
            return RedirectToPage($"./Details", new {id = Meal.RestaurantId});
        }
    }
}