﻿using System;
using System.Threading.Tasks;
using Catering.Domain;
using Catering.Infrastructure.DbContext;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Catering.Pages
{
    public class CreateModel : PageModel
    {
        private readonly SqlServerDbContext _dbContext;
        

        public CreateModel(SqlServerDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public Restaurant Restaurant { get; init; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            await _dbContext.AddAsync(Restaurant);
            await _dbContext.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
