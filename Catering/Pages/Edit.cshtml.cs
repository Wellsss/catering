﻿using System;
using System.Threading.Tasks;
using Catering.Domain;
using Catering.Infrastructure.DbContext;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace Catering.Pages
{
    public class EditModel : PageModel
    {
        private readonly SqlServerDbContext _dbContext;


        public EditModel(SqlServerDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        [BindProperty] public Restaurant Restaurant { get; set; }

        public async Task<IActionResult> OnGetAsync(long? id)
        {
            if (!id.HasValue)
            {
                return NotFound();
            }

            Restaurant = await _dbContext.RestaurantSet
                .SingleOrDefaultAsync(x => x.Id == id.Value);

            if (Restaurant is null)
            {
                return NotFound();
            }

            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            // _context.Attach(RestaurantEntity).State = EntityState.Modified;

            try
            {
                _dbContext.Update(Restaurant);
                await _dbContext.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!await RestaurantExists(Restaurant.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private async Task<bool> RestaurantExists(long id)
        {
            return await _dbContext.RestaurantSet
                .SingleOrDefaultAsync(x => x.Id == id) is not null;
        }
    }
}