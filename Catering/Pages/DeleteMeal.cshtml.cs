﻿using System;
using System.Threading.Tasks;
using Catering.Domain;
using Catering.Infrastructure.DbContext;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace Catering.Pages
{
    public class DeleteMealModel : PageModel
    {
        private readonly SqlServerDbContext _dbContext;

        public DeleteMealModel(SqlServerDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        [BindProperty] public Meal Meal { get; set; }

        public async Task<IActionResult> OnGetAsync(long? id)
        {
            if (!id.HasValue)
            {
                return NotFound();
            }

            Meal = await _dbContext.MealSet
                .Include(x => x.Restaurant)
                .SingleOrDefaultAsync(x => x.Id == id.Value);

            if (Meal is null)
            {
                return NotFound();
            }

            return Page();
        }

        public async Task<IActionResult> OnPostDeleteAsync(long id)
        {
            var meal = await _dbContext.MealSet.SingleOrDefaultAsync(x => x.Id == id);

            _dbContext.Remove(meal);
            await _dbContext.SaveChangesAsync();

            return RedirectToPage("./details", new {id = meal.RestaurantId});
        }
    }
}