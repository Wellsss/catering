﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Catering.Domain;
using Catering.Infrastructure.DbContext;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace Catering.Pages
{
    public class IndexModel : PageModel
    {
        private readonly SqlServerDbContext _dbContext;

        public IndexModel(SqlServerDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public IList<Restaurant> Restaurants { get; set; }

        public async Task OnGetAsync()
        {
            Restaurants = await _dbContext.RestaurantSet
                .AsNoTracking()
                .ToListAsync();
        }
    }
}