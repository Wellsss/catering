﻿using System;
using System.Threading.Tasks;
using Catering.Domain;
using Catering.Infrastructure.DbContext;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace Catering.Pages
{
    public class DeleteModel : PageModel
    {
        private readonly SqlServerDbContext _dbContext;

        public DeleteModel(SqlServerDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        [BindProperty] public Restaurant Restaurant { get; set; }

        public async Task<IActionResult> OnGetAsync(long? id)
        {
            if (!id.HasValue)
            {
                return NotFound();
            }

            Restaurant = await _dbContext.RestaurantSet.SingleAsync(x => x.Id == id.Value);

            if (Restaurant is null)
            {
                return NotFound();
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(long? id)
        {
            if (!id.HasValue)
            {
                return NotFound();
            }

            Restaurant = await _dbContext.RestaurantSet.SingleOrDefaultAsync(x => x.Id == id.Value);

            if (Restaurant is null)
            {
                return NotFound();
            }

            _dbContext.Remove(Restaurant);
            await _dbContext.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}