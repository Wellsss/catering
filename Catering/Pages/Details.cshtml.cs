﻿using System;
using System.Threading.Tasks;
using Catering.Domain;
using Catering.Infrastructure.DbContext;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace Catering.Pages
{
    public class DetailsModel : PageModel
    {
        private readonly SqlServerDbContext _dbContext;

        public DetailsModel(SqlServerDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public Restaurant Restaurant { get; set; }

        public async Task<IActionResult> OnGetAsync(long? id)
        {
            if (id is null)
            {
                return NotFound();
            }

            Restaurant = await _dbContext.RestaurantSet
                .Include(x => x.Meals)
                .SingleOrDefaultAsync(x => x.Id == id.Value);

            if (Restaurant is null)
            {
                return NotFound();
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAddMealAsync(Meal meal)
        {
            var restaurant = await _dbContext.RestaurantSet
                .SingleAsync(x => x.Id == meal.RestaurantId);

            restaurant.AddMeal(meal);
            await _dbContext.SaveChangesAsync();

            return RedirectToPage("./details", new {id = meal.RestaurantId});
        }
        
        public async Task<IActionResult> OnPostDeleteAsync(long mealId)
        {
            var meal = await _dbContext.MealSet.SingleOrDefaultAsync(x => x.Id == mealId);
            
            _dbContext.Remove(meal);
            await _dbContext.SaveChangesAsync();

            return RedirectToPage("./details", new {id = meal.RestaurantId});
        }
    }
}